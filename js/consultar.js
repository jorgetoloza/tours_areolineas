var data = {    
    horarios: [
        '06:00AM - 11:00AM',
        '08:00AM - 12:00PM',
        '10:00AM - 11:00AM',
        '05:00AM - 01:00PM',
        '10:00AM - 01:00PM'
    ],
    ciudades: [
        'Santa Marta',
        'New York',
        'Paris',
        'Bogota',
        'Roma',
        'Cali',
        'Mexico DF',
        'Dubai',
        'Barranquilla',
        'Seul',
        'Toronto',
        'Tokio',
        'Otawa',
        'Amsterdan'
    ]
};
var flights = [
    {
        from: 'Santa Marta',
        to: 'Dubai',
        img: 'https://www.msccroisieres.be/fr-be/Assets/Duabi-the-palm-must-see_5566_679_299-267.jpg',
        state: 'cancelado',
        cost: '3.000 US',
        airline: 'Avianca'
    },
    {
        from: 'Santa Marta',
        to: 'Bogota',
        img: 'http://www.las2orillas.co/wp-content/uploads/2015/04/bogota.jpg',
        state: 'salió',
        cost: '2.000 US',
        airline: 'Avianca'
    },
    {
        from: 'Paris',
        to: 'New York',
        img: 'https://mrfloresm.neocities.org/bloque+3/NYC.jpg',
        state: 'retrasado',
        cost: '5.000 US',
        airline: 'Continental Airlines'
    },
    {
        from: 'Mexico DF',
        to: 'Tokio',
        img: 'http://www.milenio.com/tendencias/Sviajes-Tokio-Japon-tendencias_MILIMA20160508_0007_8.jpg',
        state: 'salió',
        cost: '5.000 US',
        airline: 'Aeromexico'
    }
];
$(document).ready(function(){
    for (var i = 0; i < flights.length; i++) {
        var f = flights[i];
        var $flight = $($('#fightTemplate').html());
        $flight.find('.from').text(f.from);
        $flight.find('.to').text(f.to);
        $flight.find('.img').css('background-image', 'url(' + f.img + ')');
        $flight.find('.state').text(f.state).addClass(removeSpecials(f.state));
        $flight.find('.cost span').text(f.cost);
        $flight.find('.airline').text(f.airline);
        $('#flights').append($flight);
    }
    $('header .toggle-filters').on('click', function(){
        $(this).toggleClass('active');
        $('header .config').toggleClass('show');
        $('body').toggleClass('no-scroll');

    });
    $('#modals .header .close').on('click', function(){
        $('#modals').css('opacity', '0');
        setTimeout(function(){
            $('#modals, #modals > *').css({display: 'none', opacity: 0}).removeClass('hide');
        }, 500);
    });
    $('#flights').on('click', '.flight', function(e){
        e.preventDefault();
        $('#modals').css('display', 'flex');    
        $('#modals .book').css('display', 'block');

        setTimeout(function(){
            $('#modals').css('opacity', '1');
            $('#modals .book').css('opacity', '1');
        }, 100);
    });
    var filas = ['A', 'B', 'C', 'D', 'E'];
    for (var i = 0; i < filas.length; i++) {
        for (var j = 1; j < 8; j++) {
            $('#modals .book .seating').append('<div class="seat" data-number="' + (filas[i] + j) + '">' + (filas[i] + j) + '</div>');
        }
    }
    $('#modals .book .seat[data-number="A1"], #modals .book .seat[data-number="C4"]').addClass('occupied');
    $('#modals .book .seat').on('click', function(){
        if(!$(this).hasClass('occupied'))
            $(this).toggleClass('selected');
    });
});
function removeSpecials(str){
    return str.toLowerCase().replace(/á/gi, 'a').replace(/é/gi, 'e').replace(/í/gi, 'i').replace(/ó/gi, 'o').replace(/ú/gi, 'u')
}