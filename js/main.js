$(document).ready(function(){
	$('.select-control').each(function(){
        $select = $(this);
        if($select.attr('data-source')){
            $ul = $select.find('ul').html('');
            var array = data[$select.attr('data-source')];
            for (var i = 0; i < array.length; i++) {
                $ul.append('<div class="option">' + array[i] + '</div>');
            }
        }
    });
    $('.select-control option:selected, .select-control .option.selected').each(function(i, obj){
        $(obj).parents('.select-control').find('span').text($(obj).val());
    });
    $('.drop-down-launcher').on('click', function(){
        name = $(this).parent().attr('name');
        if(!$(this).parent().hasClass('disabled')){
            if(!$(this).siblings('ul', 'dropDown').hasClass('ocultar')){
                $(this).find('.animacionFlecha')[0].beginElement();
            }else{
                
                $('.animacionFlecha').each(function(index, obj){
                    if($(obj).parents('.select-control').attr('name') != name && !$(obj).closest('.select-control').find('ul').hasClass('ocultar')){
                        obj.beginElement();
                    	$(obj).closest('.select-control').find('ul').addClass('ocultar');
                    }
                });
                    
                $(this).find('.animacionLinea')[0].beginElement();
            }
            $(this).toggleClass('border-corner');
            $(this).siblings('ul').toggleClass('ocultar');
        }
    });
    $('.select-control').on('click', '.option', function(){
        $dropDownLauncher = $(this).parent().siblings('.drop-down-launcher');
        $(this).siblings('.option').removeClass('selected');
        $(this).addClass('selected');
        if($dropDownLauncher.parent().hasClass('autocompletado')){
            $dropDownLauncher.children('input').val($(this).text());
        }else{
            $dropDownLauncher.children('span').text($(this).text());
        }
        $dropDownLauncher.siblings('ul').addClass('ocultar');
        $dropDownLauncher.closest('.select-control').removeClass('active');
        $dropDownLauncher.find('.animacionFlecha')[0].beginElement(); 
    });
});