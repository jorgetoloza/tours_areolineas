$(document).ready(function(){
	$('#modals .header .close').on('click', function(){
        $('#modals').css('opacity', '0');
        setTimeout(function(){
            $('#modals, #modals > *').css({display: 'none', opacity: 0}).removeClass('hide');
        }, 500);
    });
    $('#verificar').on('click', function(e){
        e.preventDefault();
        $('#modals').css('display', 'flex');    
        $('#modals .book').css('display', 'block');

        setTimeout(function(){
            $('#modals').css('opacity', '1');
            $('#modals .book').css('opacity', '1');
        }, 100);
    });
    var filas = ['A', 'B', 'C', 'D', 'E'];
    for (var i = 0; i < filas.length; i++) {
        for (var j = 1; j < 8; j++) {
            $('#modals .book .seating').append('<div class="seat" data-number="' + (filas[i] + j) + '">' + (filas[i] + j) + '</div>');
        }
    }
    $('#modals .book .seat[data-number="A1"], #modals .book .seat[data-number="C4"]').addClass('occupied');
    $('#modals .book .seat').on('click', function(){
        if(!$(this).hasClass('occupied'))
            $(this).toggleClass('selected');
    });
});