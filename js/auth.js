$(document).ready(function(){
	$('.card').on('click', function(){
		var name = $(this).attr('for');
		$('#modals').css('display', 'flex');	
		$('#modals .' + name).css('display', 'block');

		setTimeout(function(){
			$('#modals').css('opacity', '1');
			$('#modals .' + name).css('opacity', '1');
		}, 100);
	});
	$('#modals .header .close').on('click', function(){
		$('#modals').css('opacity', '0');
		setTimeout(function(){
    		$('#modals, #modals > *').css({display: 'none', opacity: 0}).removeClass('hide');
		}, 500);
    });
    $('#modals .register .button').on('click', function(){
		$('#modals .register').css('opacity', '0');
		setTimeout(function(){
			$('#modals .register').addClass('hide');
			$('#modals').css('display', 'flex');	
			$('#modals .login').css('display', 'block');
			setTimeout(function(){
				$('#modals').css('opacity', '1');
				$('#modals .login').css('opacity', '1');
			}, 100);
		}, 400);
    });
});