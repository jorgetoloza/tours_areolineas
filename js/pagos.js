$(document).ready(function(){
	$('#modals .header .close').on('click', function(){
        $('#modals').css('opacity', '0');
        setTimeout(function(){
            $('#modals, #modals > *').css({display: 'none', opacity: 0}).removeClass('hide');
        }, 500);
    });
    $('#flights .flight').on('click', function(e){
        e.preventDefault();
        $('#modals').css('display', 'flex');    
        $('#modals .pay').css('display', 'block');

        setTimeout(function(){
            $('#modals').css('opacity', '1');
            $('#modals .pay').css('opacity', '1');
        }, 100);
    });
});